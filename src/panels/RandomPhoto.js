import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import persik from '../img/persik.png';

import { Button, Div, Panel, PanelHeader, PanelHeaderBack } from '@vkontakte/vkui';

const RandomPhoto = props => {
	useEffect(props.getAlbums, []);

	return (
		<Panel id={props.id}>
			<PanelHeader
				left={<PanelHeaderBack onClick={props.go} data-to="home" />}
			>
				Random Photo
			</PanelHeader>
			{props.fetchedAlbums ?
				<Div>
					{props.fetchedAlbums.items.map((elem, i) => <Div key={i} onClick={props.go} data-to="onePhoto" >{elem.title}</Div>)}
				</Div>
				:
				<div>Albums not found</div>
			}
		</Panel>
	)
};




RandomPhoto.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default RandomPhoto;
