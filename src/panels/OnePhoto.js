import React, { useEffect } from 'react';

import { Button, Div, Panel, PanelHeader, PanelHeaderBack } from '@vkontakte/vkui';


const OnePhoto = props => {

	return (
		<Panel id={props.id}>
			<PanelHeader
				left={<PanelHeaderBack onClick={props.go} data-to="randomPhoto" />}
			>
				Random Photo
			</PanelHeader>
			{ props.thePhoto ?
				<Div style={{ maxWidth: "80vw", maxHeight: "60vh", marginInline: 'auto' }}>
					<img src={props.thePhoto} alt="Some photo" style={{ display: 'block', maxWidth: "100vw", maxHeight: "75vh", marginInline: 'auto' }}/>
					<Button style={{ width: "80%", margin: "auto" }} onClick={props.getPhoto}>Next</Button>
				</Div>
				:
				<div>No photos?</div>
			}
		</Panel>
	);
}

export default OnePhoto