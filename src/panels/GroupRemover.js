import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { Avatar, Button, Cell, Div, Group, Header, Panel, PanelHeader, PanelHeaderBack } from '@vkontakte/vkui';


const GroupRemover = (props) => {

	// console.log(props.moreGroups);
	// useEffect(props.tempfunc1,[]);
	useEffect(props.getGroups, []);
	// async function fetchGroupsById() {
	// 	const groups = await bridge.send("VKWebAppCallAPIMethod",
	// 		{
	// 			"method": "groups.getById",
	// 			"params": {
	// 				"group_ids": props.fetchedGroups.items,
	// 				"access_token": token.access_token,
	// 				"v": "5.131",

	// 			}
	// 		});
	// }

	return (
		<Panel id={props.id}>
			<PanelHeader
				left={<PanelHeaderBack onClick={props.go} data-to="home" />}
			>
				GroupRemover
			</PanelHeader>

			{props.groups ?
				<Group header={<Header mode="secondary">You are subscribed on {props.groups.count} groups</Header>}>
					<Div>
						{props.groups.items.map((elem, i) => <OneGroup group={elem} notKey={i} unsubscribe={props.unsubscribe} />)}
					</Div>
					<Div>
						<Button stretched size="l" mode="secondary" onClick={props.getGroups}>
							Show more
						</Button>
					</Div>
				</Group>
				:
				<Div>No groups found</Div>
			}

		</Panel>
	)
};

const OneGroup = ({ group, notKey, unsubscribe }) => {

	return (
		<Cell
			before={group.photo ? <Avatar src={group.photo} /> : null}
			after={
				<Button mode='primary' onClick={() => unsubscribe(group.id)} style={{ marginInline: 10 }} >Unsubscribe</Button>}
			description={group.status ? group.status : ''} key={notKey}
		>
			{group.name}
		</Cell>
	);
}


GroupRemover.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
	getGroups: PropTypes.func,
	groups: PropTypes.object,
};

export default GroupRemover;
