import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import { View, ScreenSpinner, AdaptivityProvider, AppRoot, ConfigProvider, SplitLayout, SplitCol, Div } from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';

import Home from './panels/Home';
import Persik from './panels/Persik';
import persik from './img/persik.png';
import GroupRemover from './panels/GroupRemover';
import RandomPhoto from './panels/RandomPhoto';
import OnePhoto from './panels/OnePhoto';


const offsetStep = 5;

const App = () => {
	const [scheme, setScheme] = useState('bright_light')
	const [activePanel, setActivePanel] = useState('home');
	const [fetchedUser, setUser] = useState(null);
	const [fetchedGroupIds, setGroupIds] = useState(null);
	const [popout, setPopout] = useState(<ScreenSpinner size='large' />);
	const [accToken, setAccToken] = useState(null);
	// const [getMoreGroups, setMoreGroups] = useState(null); // func
	const [fetchedGroups, setGroups] = useState(null);
	const [groupsOffset, setOffset] = useState(0);
	const [fetchedAlbums, setAlbums] = useState(null);
	const [thePhoto, setPhoto] = useState(persik);


	useEffect(() => {
		bridge.subscribe(({ detail: { type, data } }) => {
			// bridge.subscribe((event) => {
			if (type === 'VKWebAppUpdateConfig') {
				setScheme(data.scheme)
			}
			// if (type === "VKWebAppAccessTokenReceived") {
			// 	setAccToken(data.access_token);
			// }
			console.log(type, data);
		});


		fetchInitData();

	}, []);


	async function fetchInitData() { // 2 request id
		const user = await bridge.send('VKWebAppGetUserInfo');
		setUser(user);

		const token = await bridge.send("VKWebAppGetAuthToken", {
			"app_id": 8212455,
			"scope": "photos, groups, friends"
		});
		setAccToken(token.access_token)

		const groups = await bridge.send("VKWebAppCallAPIMethod",
			{
				"method": "groups.get",
				"params": {
					"user_ids": user.id,
					"access_token": token.access_token,
					"v": "5.131",

				}
			});
		setGroupIds(groups.response);

		setPopout(null);
	}

	const getMoreGroups = () => {
		setPopout(<ScreenSpinner size='large' />);

		bridge.send("VKWebAppCallAPIMethod",
			{
				"method": "groups.getById",
				"params": {
					"group_ids": fetchedGroupIds.items.slice(groupsOffset, groupsOffset + offsetStep).join(", "),
					"fields": "description, status, members_count",
					"access_token": accToken,
					"v": "5.131",
				}
			})
			.then(r => {
				setGroups({
					items: r.response.map((item) => ({
						name: item.name,
						photo: item.photo_200,
						status: item.status || item.description,
						members: item.members_count,
						id: item.id
					})),
					count: fetchedGroupIds.count

				});
				setOffset(groupsOffset + offsetStep);
				setPopout(null);
			});

	}

	const unsubscribe = (groupId) => {
		setPopout(<ScreenSpinner size='large' />);
		bridge.send("VKWebAppLeaveGroup", { "group_id": groupId });
		setPopout(null);

	}

	const getAlbums = () => {
		setPopout(<ScreenSpinner size='large' />);

		bridge.send("VKWebAppCallAPIMethod",
			{
				"method": "photos.getAlbums",
				"params":
				{
					"owner_id": fetchedUser.id,
					"v": "5.131",
					"access_token": accToken,
				}
			})
			.then(r => {
				setAlbums(r.response);
				setPopout(null);
			});

	}

	const getPhoto = () => {
		//
	}

	// useEffect(() => {
	// 	fetchedGroupIds && accToken && setMoreGroups(makeTokenClosure(accToken, fetchedGroupIds));
	// }, [fetchedGroupIds]);

	const testfunc1 = () => {
		console.log(fetchedUser, accToken, fetchedGroupIds);
	}

	const go = e => {
		console.log(e.currentTarget.dataset);
		setActivePanel(e.currentTarget.dataset.to);
	};

	return (
		<ConfigProvider scheme={scheme}>
			<AdaptivityProvider>
				<AppRoot>
					<SplitLayout popout={popout}>
						<SplitCol>
							<View activePanel={activePanel}>
								<Home id='home' fetchedUser={fetchedUser} go={go} />
								<Persik id='persik' go={go} />
								<RandomPhoto id='randomPhoto' go={go} getAlbums={getAlbums} fetchedAlbums={fetchedAlbums} />
								<GroupRemover id='groupRemover' groups={fetchedGroups} unsubscribe={unsubscribe} getGroups={getMoreGroups} go={go} />
								<OnePhoto id='onePhoto' thePhoto={thePhoto} go={go} />
							</View>
						</SplitCol>
					</SplitLayout>
				</AppRoot>
			</AdaptivityProvider>
		</ConfigProvider>
	);
}

export default App;
